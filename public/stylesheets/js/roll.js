var app;

(function() {

  app = {
    settings: {

      elements: {},

      rand: '',

      configs: {
        string: "Appuyer sur le bouton pour lancer le dés",
        choiceNumber: 6,
        loop: 0,
        howManyTimes: 67,
        array_hexagon_suite: [1, 3, 5, 4, 2, 0],
        index_hexagon_suite: 0,
        delay_hexagon_suite: 50,
        array_Color_hexagon_suite: ['#2d383c', '#5b666a', '#9aa2a5', '#b0b5b9', '#c7cbce', '#dfe3e6'],
        color_hexagon_suite: '#2d383c',
      },

    },

    init: function() {
      app.parseStringContentText();
      app.displayChoiceBlock(confs.choiceNumber);
    },
    recup_get: function (param) {
      var vars = {};
      window.location.href.replace( location.hash, '' ).replace(
        /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
        function( m, key, value ) { // callback
          vars[key] = value !== undefined ? value : '';
        }
      );

      if ( param ) {
        return vars[param] ? vars[param] : null;
      }
      return vars;
    },
    parseStringContentText: function() {

      if (app.settings.configs.string.length > 900) {
        document.getElementById('text_page').innerHTML = info;
      }
      var segments = app.settings.configs.string.match(/.{1,180}/g);

      for (var i = 0; i < segments.length; i++) {
        var elDiv = document.createElement('div');
        elDiv.className = 'carousel-item text-center p-4';

        if (i == 0) {
          elDiv.className += ' active';
        }
        var elP = document.createElement('p');
        elP.textContent = segments[i];
        elDiv.appendChild(elP);
        document.getElementsByClassName('carousel-inner')[0].appendChild(elDiv);
      }
    },

    displayChoiceBlock: function(number) {
      var disabled = 6 - number;

      for (var i = 0; i < disabled; i++) {
        if (document.getElementsByClassName('choice_block')) {
          document.getElementsByClassName('choice_block')[i].className += ' disabled';
        }
      }
    },

    launchRoll: function(number) {
      document.getElementsByClassName('carousel-item')[0].getElementsByTagName('p')[0].innerHTML = 'Les dés sont jetés';
      app.newRoll();
    },

    launchRollFight: function(number) {
      document.getElementsByClassName('carousel-item')[0].getElementsByTagName('p')[0].innerHTML = 'Les dés sont jetés';
      app.newRollFight();
    },

    newRoll: function() {
      confs.loop++;

      if (confs.loop < confs.howManyTimes) {
        document.getElementsByTagName('path')[confs.array_hexagon_suite[confs.index_hexagon_suite]].style.fill = confs.color_hexagon_suite;
        confs.index_hexagon_suite++;

        if (confs.loop > 55) {
          confs.delay_hexagon_suite += 50;
        }

        if (confs.index_hexagon_suite == 6) {
          confs.index_hexagon_suite = 0;
          var before_color_hexagon_suite = confs.color_hexagon_suite;
          var after_color_hexagon_suite = confs.array_Color_hexagon_suite[app.randomIntFromInterval(0, 5)];

          while (before_color_hexagon_suite == after_color_hexagon_suite) {
            after_color_hexagon_suite = confs.array_Color_hexagon_suite[app.randomIntFromInterval(0, 5)];
          }
          confs.color_hexagon_suite = after_color_hexagon_suite;
        }

        setTimeout(function() {
          app.newRoll();
        }, confs.delay_hexagon_suite);

      } else {
        var rand = app.randomIntFromInterval(2, 12);
        var valeur0 = app.recup_get('1');
        var valeur1 = app.recup_get('0');

        if (app.settings.rand == rand) {
          app.settings.rand = rand;
          document.getElementsByClassName('carousel-item')[0].getElementsByTagName('p')[0].innerHTML = 'Vous avez encore obtenu un ' + app.settings.rand + '<br/>';
        } else {
          app.settings.rand = rand;
          document.getElementsByClassName('carousel-item')[0].getElementsByTagName('p')[0].innerHTML = 'Vous avez obtenu un ' + app.settings.rand + '<br/>';

          if (rand >= stats_chance) {
            document.getElementsByClassName('carousel-item')[0].getElementsByTagName('p')[0].innerHTML += "Vous n'avez pas de chance. <br/>";
            document.getElementById('content_buttom_next_action').innerHTML = '<a href="page' + valeur0 +  link + '"><button type="button" class="btn btn-outline-secondary">Continuer</button></a>';

          }else if (rand < stats_chance) {
            document.getElementsByClassName('carousel-item')[0].getElementsByTagName('p')[0].innerHTML += "Vous avez de la chance. <br/>";
            document.getElementById('content_buttom_next_action').innerHTML = '<a href="page' + valeur1 + link + '"><button type="button" class="btn btn-outline-secondary">Continuer</button></a>';
          }
          document.getElementsByClassName('chance')[0].innerText = parseInt(document.getElementsByClassName('chance')[0].innerText) - 1;
        }
        document.getElementsByClassName('carousel-item')[0].getElementsByTagName('p')[0].innerHTML += "Vous perdez un point de chance. ";
        document.getElementsByClassName('contentRandNumber')[0].innerHTML = app.settings.rand;
      }

    },

    newRollFight: function() {
      confs.loop++;

      if (confs.loop < confs.howManyTimes) {
        // document.getElementById('content_super_hexagon_bad')
        var content_super_hexagon_bad = document.getElementById('content_super_hexagon_bad');
        content_super_hexagon_bad.getElementsByTagName('path')[confs.array_hexagon_suite[confs.index_hexagon_suite]].style.fill = confs.color_hexagon_suite;
        confs.index_hexagon_suite++;

        if (confs.loop > 55) {
          confs.delay_hexagon_suite += 50;
        }

        if (confs.index_hexagon_suite == 6) {
          confs.index_hexagon_suite = 0;
          var before_color_hexagon_suite = confs.color_hexagon_suite;
          var after_color_hexagon_suite = confs.array_Color_hexagon_suite[app.randomIntFromInterval(0, 5)];

          while (before_color_hexagon_suite == after_color_hexagon_suite) {
            after_color_hexagon_suite = confs.array_Color_hexagon_suite[app.randomIntFromInterval(0, 5)];
          }
          confs.color_hexagon_suite = after_color_hexagon_suite;
        }

        setTimeout(function() {
          app.newRollFight();
        }, confs.delay_hexagon_suite);

      } else {
        var randPlayer = app.randomIntFromInterval(2, 12);
        var randMonster = app.randomIntFromInterval(2, 12);
        var valeur0 = app.recup_get('1');
        var valeur1 = app.recup_get('0');

        if (app.settings.rand == randPlayer) {
          app.settings.rand = randPlayer;
          document.getElementsByClassName('carousel-item')[0].getElementsByTagName('p')[0].innerHTML = 'Vous avez encore obtenu un ' + app.settings.rand + '<br/>';
          document.getElementsByClassName('carousel-item')[0].getElementsByTagName('p')[0].innerHTML += 'Le Monstre 1 à obtenu un ' + randMonster + '<br/>';
        } else {
          app.settings.rand = randPlayer;
          document.getElementsByClassName('carousel-item')[0].getElementsByTagName('p')[0].innerHTML = 'Vous avez obtenu un ' + app.settings.rand + '<br/>';
          document.getElementsByClassName('carousel-item')[0].getElementsByTagName('p')[0].innerHTML += 'L\'adversaire à obtenu un ' + randMonster + '<br/>';

          // if (rand >= stats_chance) {
          //   document.getElementsByClassName('carousel-item')[0].getElementsByTagName('p')[0].innerHTML += "Vous n'avez pas de chance. <br/>";
          //   document.getElementById('content_buttom_next_action').innerHTML = '<a href="page' + valeur0 + '"><button type="button" class="btn btn-outline-secondary">Continuer</button></a>';
          //
          // }else if (rand < stats_chance) {
          //   document.getElementsByClassName('carousel-item')[0].getElementsByTagName('p')[0].innerHTML += "Vous avez de la chance. <br/>";
          //   document.getElementById('content_buttom_next_action').innerHTML = '<a href="page' + valeur1 + '"><button type="button" class="btn btn-outline-secondary">Continuer</button></a>';
          // }
          //
        }
        document.getElementsByClassName('contentTextMonster')[0].innerHTML = randMonster;
        document.getElementsByClassName('contentTextPlayer')[0].innerHTML = randPlayer;

        document.getElementsByClassName('contentRandNumber')[0].innerHTML = randPlayer;
        document.getElementsByClassName('contentRandNumber')[1].innerHTML = randMonster;
      }

    },

    randomIntFromInterval: function(min, max) {
      return Math.floor(Math.random() * (max - min + 1) + min);
    },
  };

  var confs = app.settings.configs;
  app.init();

})();
