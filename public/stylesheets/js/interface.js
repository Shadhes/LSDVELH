var app;

(function () {

    app = {
        settings: {

          elements: {
            // elButtonCircleRight: document.getElementById('circle_right'),
            // elButtonCircleLeft: document.getElementById('circle_left'),
            // elButtonInventoryWeapon: document.getElementById('inventory_weapon'),
            // elButtonInventoryObject: document.getElementById('inventory_object'),
          },

          configs: {
            // string : document.getElementById('texte').value,
            choiceNumber : 4,
          },

      },

      init: function() {
        // app.parseStringContentText();
        app.displayChoiceBlock(app.settings.configs.choiceNumber);
      },

      parseStringContentText: function() {
        if (app.settings.configs.string.length > 900) {
          document.getElementById('text_page').innerHTML = info;
        }
        var segments = app.settings.configs.string.match(/.{1,180}/g);
        for (var i = 0; i < segments.length; i++)
        {
          var elDiv = document.createElement('div');
          elDiv.className = 'carousel-item text-center p-4';
          if (i == 0) {
            elDiv.className += ' active';
          }
          var elP = document.createElement('p');
          elP.textContent = segments[i];
          elDiv.appendChild(elP);
          document.getElementsByClassName('carousel-inner')[0].appendChild(elDiv);
        }
      },

      displayChoiceBlock: function(number) {
        // console.log( document.getElementsByClassName('choice_block'));
        var disabled = 6 - number;
        // console.log(disabled);
        if (document.getElementsByClassName('choice_block') == true) {
          for (var i = 0; i < disabled; i++) {
              document.getElementsByClassName('choice_block')[i].className += ' disabled';
          }
        }
      },

  };

  app.init();

  // console.log('here', html);
  // document.getElementById('text_page').innerHTML = html.textContent;

    // function displayInventory(open, closed){
  //   if (document.getElementById(open).style.display == 'block') {
  //     document.getElementById(open).style.display = 'none';
  //     return;
  //   }
  //   document.getElementById(closed).style.display = 'none';
  //   document.getElementById(open).style.display = 'block';
  // }
}) ();
