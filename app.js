var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// nodemon --exec npm start

var indexRouter = require('./routes/index');
var rollRouter = require('./routes/roll');
var textRouter = require('./routes/text');
var standardChoice = require('./routes/standardChoice');
var combatChoice = require('./routes/combat');

var introRouter = require('./routes/pageIntro');
var page001Router = require('./routes/page001');
var page020Router = require('./routes/page020');
var page300Router = require('./routes/page300');
var page039Router = require('./routes/page039');
var page141Router = require('./routes/page141');
var page234Router = require('./routes/page234');
var page058Router = require('./routes/page058');
var page278Router = require('./routes/page278');
var page093Router = require('./routes/page093');
var page077Router = require('./routes/page077');
var page103Router = require('./routes/page103');
var page192Router = require('./routes/page192');
var page228Router = require('./routes/page228');
var page063Router = require('./routes/page063');
var page318Router = require('./routes/page318');
var page134Router = require('./routes/page134');

var endRouter = require('./routes/end');

var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/roll', rollRouter);
app.use('/text', textRouter);
app.use('/standardChoice', standardChoice);
app.use('/combat', combatChoice);

app.use('/pageIntro', introRouter);

app.use('/page001', page001Router);
app.use('/page020', page020Router);
app.use('/page300', page300Router);
app.use('/page039', page039Router);
app.use('/page141', page141Router);
app.use('/page234', page234Router);
app.use('/page058', page058Router);
app.use('/page278', page278Router);
app.use('/page093', page093Router);
app.use('/page077', page077Router);
app.use('/page103', page103Router);
app.use('/page192', page192Router);
app.use('/page228', page228Router);
app.use('/page063', page063Router);
app.use('/page318', page318Router);
app.use('/page134', page134Router);

app.use('/end', endRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
