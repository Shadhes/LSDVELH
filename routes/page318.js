var express = require('express');
var router = express.Router();
var confs = require('../configs')

/* GET home page. */
router.get('/', function(req, res, next) {
  link = 'banana';
  string = "Le deuxième cachot paraît vide. Mais lorsque vous y pénétrez, une petite boule de fourrure hurlante et dotée de quatre pattes bondit de derrière la porte sur vos épaules. Un Gyryx ! c'est un Gyryx ! le plus effroyable rongeur de l'univers ! Vous parvenez à vous en débarrasser en lui assénant un coup de crosse de votre Désintégrateur, mais pas assez rapidement, cependant, pour l'empêcher d'entamer de ses redoutables dents votre Combinaison Spatiale. Vous perdez 1 point de PROTECTION. Le Gyryx va s'écraser contre un mur, mais il parvient néanmoins à se remettre sur ses pattes et vous fixe de ses gros yeux rouges avec l'air le plus méchant qui soit. Si vous ne l'avez pas encore fait, vous pouvez maintenant regarder ce qui se trouve dans l'autre cachot. Sinon quittez la pièce par l'écoutille et revenez dans le tunnel.";
  var stats = '?habilite=' + req.query.habilite + '&endurance=' + req.query.endurance + '&protection=' + req.query.protection + '&chance=' + req.query.chance + '';
  var stats_trap = '?habilite=' + req.query.habilite + '&endurance=' + req.query.endurance + '&protection=' + req.query.protection - 1 + '&chance=' + req.query.chance + '';
  var items = '&sli01=' + req.query.sli01 + '&sli03=' + req.query.sli02 + '&sli03=' + req.query.sli03 + '&sli04=' + req.query.sli04 + '&sli05=' + req.query.sli05 + '&sli06=' + req.query.sli06 + '';

  res.render('standardChoice',{
    message: string,
    image_url : 'images/black-b.jpg',
    choice01 : {
      image_url : false,
      text : 'Regarder dans le second cachot',
      link : '/page063' + stats + items,
    },
    choice02 : {
      image_url : false,
      text : 'Revenez dans le tunnel',
      link : '/page134' + stats_trap + items,
    },
    choice03 : {
      image_url : false,
      text : '',
      link : '',
    },
    choice04 : {
      image_url : false,
      text : '',
      link : '',
    },
    choice05 : {
      image_url : false,
      text : '',
      link : '',
    },
    choice06 : {
      image_url : false,
      text : '',
      link : '',
    },
    link : link,
    statistique : {
      stats_habilite : req.query.habilite,
      stats_endurance :  req.query.endurance,
      stats_protection :  req.query.protection,
      stats_chance :  req.query.chance,
    },
    object : {
      // ?habilite=10&endurance=12&protection=8&chance=6
      weapon : {
        slot01_weapon : req.query.sl_w_01,
        slot02_weapon :  req.query.sl_w_02,
        slot03_weapon :  req.query.sl_w_03,
        slot04_weapon :  req.query.sl_w_04,
        slot05_weapon :  req.query.sl_w_05,
        slot06_weapon :  req.query.sl_w_06,
      },
      items : {
        slot01_item : req.query.sli01,
        slot02_item :  req.query.sli02,
        slot03_item :  req.query.sli03,
        slot04_item :  req.query.sli04,
        slot05_item :  req.query.sli05,
        slot06_item :  req.query.sli06,
      },
    },
  });

});

module.exports = router;
