var express = require('express');
var router = express.Router();
var confs = require('../configs')

/* GET home page. */
router.get('/', function(req, res, next) {
  link = '';
  string = "FIGHT";

  res.render('combat',{
    message: string,
    choice01 : {
      text : 'nada',
      link : '',
    },
    choice02 : {
      text : '',
      link : '',
    },
    choice03 : {
      text : '',
      link : '',
    },
    choice04 : {
      text : '',
      link : '',
    },
    choice05 : {
      text : '',
      link : '',
    },
    choice06 : {
      text : '',
      link : '',
    },
    link : link,
    statistique : {
      stats_habilite : confs.stats_habilite,
      stats_endurance :  confs.stats_endurance,
      stats_protection :  confs.stats_protection,
      stats_chance :  confs.stats_chance,
    },
    object : {
      // ?habilite=10&endurance=12&protection=8&chance=6
      weapon : {
        slot01_weapon : req.query.sl_w_01,
        slot02_weapon :  req.query.sl_w_02,
        slot03_weapon :  req.query.sl_w_03,
        slot04_weapon :  req.query.sl_w_04,
        slot05_weapon :  req.query.sl_w_05,
        slot06_weapon :  req.query.sl_w_06,
      },
      items : {
        slot01_item : req.query.sli01,
        slot02_item :  req.query.sli02,
        slot03_item :  req.query.sli03,
        slot04_item :  req.query.sli04,
        slot05_item :  req.query.sli05,
        slot06_item :  req.query.sli06,
      },
    },
    monster : {
      name : 'GARDEROBOT',
      stats_habilite : 05,
      stats_endurance :  05,
    },
  });

});

module.exports = router;
