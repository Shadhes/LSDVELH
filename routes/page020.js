var express = require('express');
var router = express.Router();
var confs = require('../configs')

/* GET home page. */
router.get('/', function(req, res, next) {
  link = '';
  string = "FIGHT";

  var stats = '?habilite=' + req.query.habilite + '&endurance=' + req.query.endurance + '&protection=' + req.query.protection + '&chance=' + req.query.chance + '';
  var items = '&sli01=' + req.query.sli01 + '&sli02=' + req.query.sli02 + '&sli03=' + req.query.sli03 + '&sli04=' + req.query.sli04 + '&sli05=' + req.query.sli05 + '&sli06=' + req.query.sli06 + '';

  if (req.query.sli01 == 'charge_antimatière' || req.query.sli02 == 'charge_antimatière' || req.query.sli03 == 'charge_antimatière' || req.query.sli04 == 'charge_antimatière' || req.query.sli05 == 'charge_antimatière' || req.query.sli06 == 'charge_antimatière'){

    res.render('combat',{
      message: string,
      image_url : 'images/black-b.jpg',
      choice01 : {
        image_url : false,
        text : '',
        link : '',
      },
      choice02 : {
        image_url : false,
        text : '',
        link : '',
      },
      choice03 : {
        image_url : false,
        text : '',
        link : '',
      },
      choice04 : {
        image_url : false,
        text : '',
        link : '',
      },
      choice05 : {
        image_url : false,
        text : '',
        link : '',
      },
      choice06 : {
        image_url : false,
        text : '',
        link : '',
      },
      link : link,
      statistique : {
        stats_habilite : req.query.habilite,
        stats_endurance :  req.query.endurance,
        stats_protection :  req.query.protection,
        stats_chance :  req.query.chance,
      },
      object : {
        // ?habilite=10&endurance=12&protection=8&chance=6
        weapon : {
          slot01_weapon : req.query.sl_w_01,
          slot02_weapon :  req.query.sl_w_02,
          slot03_weapon :  req.query.sl_w_03,
          slot04_weapon :  req.query.sl_w_04,
          slot05_weapon :  req.query.sl_w_05,
          slot06_weapon :  req.query.sl_w_06,
        },
        items : {
          slot01_item : req.query.sli01,
          slot02_item :  req.query.sli02,
          slot03_item :  req.query.sli03,
          slot04_item :  req.query.sli04,
          slot05_item :  req.query.sli05,
          slot06_item :  req.query.sli06,
        },
      },
      monster : {
        name : 'GARDEROBOT',
        stats_habilite : 05,
        stats_endurance :  05,
      },
    });
  }else{
    res.redirect('page001?habilite=10&endurance=12&protection=8&chance=4&sli01=&sli02=&sli03=&sli04=&sli05=&sli06=&err=true')
  }

});

module.exports = router;
