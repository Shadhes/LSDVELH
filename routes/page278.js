var express = require('express');
var router = express.Router();
var confs = require('../configs')

/* GET home page. */
router.get('/', function(req, res, next) {
  link = 'banana';
  string = "Vous vous reculez à temps pour éviter les câbles et les étincelles qui en jaillissent. De toute évidence, il vaut mieux ne pas tenter de franchir cette écoutille, et vous vous dirigez donc vers l'autre.";
  var stats = '?habilite=' + req.query.habilite + '&endurance=' + req.query.endurance + '&protection=' + req.query.protection + '&chance=' + req.query.chance + '';
  var items = '&sli01=' + req.query.sli01 + '&sli03=' + req.query.sli02 + '&sli03=' + req.query.sli03 + '&sli04=' + req.query.sli04 + '&sli05=' + req.query.sli05 + '&sli06=' + req.query.sli06 + '';

  res.render('standardChoice',{
    message: string,
    image_url : 'images/black-b.jpg',
    choice01 : {
      image_url : false,
      text : 'Ouvrir l’écoutille',
      link : '/page103' + stats + items,
    },
    choice02 : {
      image_url : false,
      text : 'Continuer dans le tunnel',
      link : '/page134' + stats + items,
    },
    choice03 : {
      image_url : false,
      text : '',
      link : '',
    },
    choice04 : {
      image_url : false,
      text : '',
      link : '',
    },
    choice05 : {
      image_url : false,
      text : '',
      link : '',
    },
    choice06 : {
      image_url : false,
      text : '',
      link : '',
    },
    link : link,
    statistique : {
      stats_habilite : req.query.habilite,
      stats_endurance :  req.query.endurance,
      stats_protection :  req.query.protection,
      stats_chance :  req.query.chance,
    },
    object : {
      // ?habilite=10&endurance=12&protection=8&chance=6
      weapon : {
        slot01_weapon : req.query.sl_w_01,
        slot02_weapon :  req.query.sl_w_02,
        slot03_weapon :  req.query.sl_w_03,
        slot04_weapon :  req.query.sl_w_04,
        slot05_weapon :  req.query.sl_w_05,
        slot06_weapon :  req.query.sl_w_06,
      },
      items : {
        slot01_item : req.query.sli01,
        slot02_item :  req.query.sli02,
        slot03_item :  req.query.sli03,
        slot04_item :  req.query.sli04,
        slot05_item :  req.query.sli05,
        slot06_item :  req.query.sli06,
      },
    },
  });

});

module.exports = router;
