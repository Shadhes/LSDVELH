var express = require('express');
var router = express.Router();
var confs = require('../configs')


/* GET home page. */


router.get('/', function(req, res, next) {
  link = 'banana';
  string = "Sur votre droite et sur votre gauche, vous remarquez deux écoutilles, fermées également, et sur lesquelles vous pouvez lire la mention « Danger » inscrite en gros caractères. Sur le sol, non loin de la porte, se trouve un petit tas de détritus, d'origine organique, semble-t-il. Si vous possédez une Charge d'Antimatière, vous pouvez l'utiliser contre la porte de sécurité. Mais vous pouvez aussi examiner une des deux écoutilles ou vous intéresser au tas de détritus.";
  var stats = '?habilite=' + req.query.habilite + '&endurance=' + req.query.endurance + '&protection=' + req.query.protection + '&chance=' + req.query.chance + '';
  var items = '&sli01=' + req.query.sli01 + '&sli02=' + req.query.sli02 + '&sli03=' + req.query.sli03 + '&sli04=' + req.query.sli04 + '&sli05=' + req.query.sli05 + '&sli06=' + req.query.sli06 + '';
  var err = req.query.err;

  if (err == 'true') {
    string = 'Vous ne disposez pas de charge antimatière.'
  }
  res.render('standardChoice',{
    message: string,
    image_url : 'images/page01/page01_01.jpg',
    choice01 : {
      text : 'Utiliser une Charge ATM sur la porte',
      link : '/page020' + stats + items,
      image_url : 'images/page01/page01_05.jpg',
    },
    choice02 : {
      text : 'Ouvrir l’écoutille de gauche',
      link : 'page058' + stats + items,
      image_url : 'images/page01/page01_04.jpg',
    },
    choice03 : {
      text : ' Ouvrir l’écoutille de droite',
      link : 'page077' + stats + items,
      image_url : 'images/page01/page01_02.jpg',
    },
    choice04 : {
      text : 'Regarder le tas de détritus',
      link : 'page039' + stats + items,
      image_url : 'images/page01/page01_03.jpg',
    },
    choice05 : {
      text : '',
      link : '',
      image_url : false,
    },
    choice06 : {
      text : '',
      link : '',
      image_url : false,
    },
    link : link,
    statistique : {
      // ?habilite=10&endurance=12&protection=8&chance=6
      stats_habilite : req.query.habilite,
      stats_endurance :  req.query.endurance,
      stats_protection :  req.query.protection,
      stats_chance :  req.query.chance,
    },
    object : {
      // ?habilite=10&endurance=12&protection=8&chance=6
      weapon : {
        slot01_weapon : req.query.sl_w_01,
        slot02_weapon :  req.query.sl_w_02,
        slot03_weapon :  req.query.sl_w_03,
        slot04_weapon :  req.query.sl_w_04,
        slot05_weapon :  req.query.sl_w_05,
        slot06_weapon :  req.query.sl_w_06,
      },
      items : {
        slot01_item : req.query.sli01,
        slot02_item :  req.query.sli02,
        slot03_item :  req.query.sli03,
        slot04_item :  req.query.sli04,
        slot05_item :  req.query.sli05,
        slot06_item :  req.query.sli06,
      },
    },
  });

});

module.exports = router;
