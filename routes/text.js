var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {

  var string = "";
  var link = "";
  var stats = '?habilite=' + req.query.habilite + '&endurance=' + req.query.endurance + '&protection=' + req.query.protection + '&chance=' + req.query.chance + '';
  var items = '&sli01=' + req.query.sli01 + '&sli03=' + req.query.sli02 + '&sli03=' + req.query.sli03 + '&sli04=' + req.query.sli04 + '&sli05=' + req.query.sli05 + '&sli06=' + req.query.sli06 + '';

    link = 'standardChoice';
    image_url : 'images/black-b.jpg',
    string = "Depuis quelque temps, Cyrus, le savant tyrannique, souverain de la constellation d'Od - à laquelle vous appartenez -, harcelle votre planète natale avec ses créatures démoniaques : robots destructeurs et êtres diaboliques, d'espèces certainement mutantes. Sa pratique criminelle la plus courante est de les envoyer sur votre planète, afin qu'ils enlèvent d'innocentes victimes pour les soumettre, si l'on en croit la rumeur, à des expériences chirurgicales innommables. Quoi qu'il en \ soit, aucun de ces malheureux n'a jamais été revu, mort ou vivant. Et maintenant, la nouvelle court que Cyrus a l'intention de soumettre la planète tout entière à une épouvantable expérience biologique, consistant à en recouvrir la surface d'isotopes radioactifs, puis à contaminer ensuite ses habitants avec des virus mortels. Manifestement, le temps est venu de mettre un terme aux agissements de Cyrus, le savant dément. C'est dans ce but que les autorités de votre planète ont fait appel à \ la Guilde des Mercenaires de l'Espace. Et c'est VOUS qui avez été  choisi, avec pour mission de pénétrer dans le vaisseau intergalactique, le Vandervecken, et de capturer Cyrus afin qu'il soit livré à la justice. Équipé des armes les plus perfectionnées et les plus mortelles, entraîné dans vingt-sept différents arts martiaux, tant humains que pratiqués par d'étranges et lointaines civilisations, protégé par la Combinaison Spatiale Sensorielle la plus sophistiquée, vous vous mettez en chasse, à la recherche du Vandervecken Tout d'abord, vous explorez sans succès le système d'étoiles auquel votre planète appartient. Et, finalement, dans une constellation lointaine et relativement isolée, située à des années-lumière de votre propre monde, vous finissez par découvrir le sombre et formidable vaisseau. Il semble qu'il ait fait escale à proximité d'une petite planète, afin de s'y ravitailler en vivres et en énergie. La meilleure décision à prendre est certainement de vous glisser à bord de l'aéronef ravitailleur, qui vous mènera alors, sans difficulté, jusqu'au Vandervecken. \
    Dissimulé dans les soutes de l'aéronef ravitailleur, vous quittez bientôt la surface de la planète en direction du vaisseau intergalactique. Alors que l'aéronef s'en approche, vous vous hissez dans un sas de secours et vous tendez l'oreille à l'écoute du choc de l'abordage des deux vaisseaux. Vous vous assurez du bon fonctionnement de vos armes et de votre Combinaison Sensorielle, puis vous saisissez la poignée commandant le système de déverrouillage du sas qui s'ouvrira au moindre de vos mouvements et vous projettera dans le vide. Dans vos écouteurs, vous pouvez entendre le compte à rebours de l'ordinateur qui égrène les secondes précédant l'amarrage. A zéro, vous actionnez la poignée et vous vous retrouvez flottant dans l'espace, non loin de la coque d'argent du Vandervecken.Derrière vous, le ravitailleur semble une minuscule flèche plantée dans une monstrueuse cible. Maintenant, vous progressez lentement dans l'immensité interstellaire, à quelques mètres au-dessus du vaisseau. Un détail que vous remarquez sur sa coque attire soudain votre attention : sur la surface lisse en effet - faisant légèrement saillie -, se détache une forme ovale, de couleur sombre. Sans aucun doute s'agit-il d'un sas qui pourrait vous permettre de pénétrer dans le Vandervecken. \
    Sans perdre un instant, vous vous amarrez au vaisseau à l'aide d'un grappin magnétique et vous vous approchez du sas pour l'examiner. Le mécanisme d'ouverture ne semble pas présenter de grandes difficultés. Et, en effet, après quelques tâtonnements, la porte semble frémir, puis elle s'ouvre lentement. Il vous faut peu de temps pour pénétrer à l'intérieur du sas, en refermer l'ouverture et l'emplir d'une atmosphère respirable. Vous voilà maintenant à pied d'œuvre. Quittant le sas, vous vous retrouvez à l'extrémité d'un petit corridor dont l'autre extrémité est fermée par une lourde porte de sécurité.";

  res.render('text',{
    message: string,
    link: link,
  });

});


module.exports = router;
