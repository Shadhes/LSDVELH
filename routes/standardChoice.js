var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  link = 'banana';
  string = "Sur votre droite et sur votre gauche, vous remarquez deux écoutilles, fermées également, et sur lesquelles vous pouvez lire la mention « Danger » inscrite en gros caractères. Sur le sol, non loin de la porte, se trouve un petit tas de détritus, d'origine organique, semble-t-il. Si vous possédez une Charge d'Antimatière, vous pouvez l'utiliser contre la porte de sécurité. Mais vous pouvez aussi examiner une des deux écoutilles ou vous intéresser au tas de détritus.";
  var items = '&sli01=' + req.query.sli01 + '&sli03=' + req.query.sli02 + '&sli03=' + req.query.sli03 + '&sli04=' + req.query.sli04 + '&sli05=' + req.query.sli05 + '&sli06=' + req.query.sli06 + '';
  var stats = '?habilite=' + req.query.habilite + '&endurance=' + req.query.endurance + '&protection=' + req.query.protection + '&chance=' + req.query.chance + '';

  res.render('standardChoice',{
    message: string,
    choice01 : {
      text : 'Utiliser une Charge Antimatière sur la porte de sécurité',
      link : '/page_020',
    },
    choice02 : {
      text : 'Ouvrir l’écoutille de gauche',
      link : '',
    },
    choice03 : {
      text : ' Ouvrir l’écoutille de droite',
      link : '',
    },
    choice04 : {
      text : 'Regarder le tas de détritus',
      link : '',
    },
    choice05 : {
      text : '',
      link : '',
    },
    choice06 : {
      text : '',
      link : '',
    },
    link : link,
    statistique : {
      stats_habilite : confs.stats_habilite,
      stats_endurance :  confs.stats_endurance,
      stats_protection :  confs.stats_protection,
      stats_chance :  confs.stats_chance,
    },
    object : {
      // ?habilite=10&endurance=12&protection=8&chance=6
      weapon : {
        slot01_weapon : req.query.sl_w_01,
        slot02_weapon :  req.query.sl_w_02,
        slot03_weapon :  req.query.sl_w_03,
        slot04_weapon :  req.query.sl_w_04,
        slot05_weapon :  req.query.sl_w_05,
        slot06_weapon :  req.query.sl_w_06,
      },
      items : {
        slot01_item : req.query.sl_i_01,
        slot02_item :  req.query.sl_i_02,
        slot03_item :  req.query.sl_i_03,
        slot04_item :  req.query.sl_i_04,
        slot05_item :  req.query.sl_i_05,
        slot06_item :  req.query.sl_i_06,
      },
    },
  });

});

module.exports = router;
