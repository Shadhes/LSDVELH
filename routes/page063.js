var express = require('express');
var router = express.Router();
var confs = require('../configs')

/* GET home page. */
router.get('/', function(req, res, next) {
  link = 'banana';
  string = "Le cachot est occupé par un vieil homme au visage couvert de meurtrissures, aux bras couturés de cicatrices, et dont le reste du corps disparaît sous d'épais bandages. Il vous regarde avec un air apeuré. Mais à peine a-t-il aperçu les restes fumants du robot derrière vous que son visage s'illumine. A mots précipités, il vous raconte qu'il a été fait prisonnier voilà longtemps par Cyrus qui ne cesse, depuis, de le torturer. Les yeux brillants, il vous souhaite de mener à bien votre mission. Malheureusement, il ne peut pas vous donner beaucoup de renseignements sur le Vandervecken, dont il a, cependant, rencontré le pilote : « ... Une drôle de machine ! Toujours à se poser des questions et à se torturer les mécanismes. A mon avis, si vous le rencontrez et qu'il vous demande quoi que ce soit, vous feriez mieux de lui répondre que vous n'en savez rien. Certainement la meilleure façon de vous en sortir... » Si vous ne l'avez déjà fait, vous pouvez aller examiner l'autre cachot. Sinon, vous vous éloignez en repassant par l'écoutille d'entretien et le couloir.";
  var stats = '?habilite=' + req.query.habilite + '&endurance=' + req.query.endurance + '&protection=' + req.query.protection + '&chance=' + req.query.chance + '';
  var items = '&sli01=' + req.query.sli01 + '&sli03=' + req.query.sli02 + '&sli03=' + req.query.sli03 + '&sli04=' + req.query.sli04 + '&sli05=' + req.query.sli05 + '&sli06=' + req.query.sli06 + '';

  res.render('standardChoice',{
    message: string,
    image_url : 'images/black-b.jpg',
    choice01 : {
      image_url : false,
      text : 'Examiner l’autre cachot',
      link : '/page308' + stats + items,
    },
    choice02 : {
      image_url : false,
      text : 'Retourner dans le couloir',
      link : '/page134' + stats + items,
    },
    choice03 : {
      image_url : false,
      text : '',
      link : '',
    },
    choice04 : {
      image_url : false,
      text : '',
      link : '',
    },
    choice05 : {
      image_url : false,
      text : '',
      link : '',
    },
    choice06 : {
      image_url : false,
      text : '',
      link : '',
    },
    link : link,
    statistique : {
      stats_habilite : req.query.habilite,
      stats_endurance :  req.query.endurance,
      stats_protection :  req.query.protection,
      stats_chance :  req.query.chance,
    },
    object : {
      // ?habilite=10&endurance=12&protection=8&chance=6
      weapon : {
        slot01_weapon : req.query.sl_w_01,
        slot02_weapon :  req.query.sl_w_02,
        slot03_weapon :  req.query.sl_w_03,
        slot04_weapon :  req.query.sl_w_04,
        slot05_weapon :  req.query.sl_w_05,
        slot06_weapon :  req.query.sl_w_06,
      },
      items : {
        slot01_item : req.query.sli01,
        slot02_item :  req.query.sli02,
        slot03_item :  req.query.sli03,
        slot04_item :  req.query.sli04,
        slot05_item :  req.query.sli05,
        slot06_item :  req.query.sli06,
      },
    },
  });

});

module.exports = router;
