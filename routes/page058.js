var express = require('express');
var router = express.Router();
var confs = require('../configs')

/* GET home page. */
router.get('/', function(req, res, next) {

  var string = "";
  var link = "";
  var stats = '&habilite=' + req.query.habilite + '&endurance=' + req.query.endurance + '&protection=' + req.query.protection + '&chance=' + req.query.chance + '';
  var items = '&sli01=' + req.query.sli01 + '&sli03=' + req.query.sli02 + '&sli03=' + req.query.sli03 + '&sli04=' + req.query.sli04 + '&sli05=' + req.query.sli05 + '&sli06=' + req.query.sli06 + '';

  link = 'roll?0=093&1=278' + stats + items;
  text_buttom = 'Tentez votre chance';
  string = "Non sans mal, vous parvenez à ouvrir l'écoutille. Mais la négligence du technicien qui l'a installée fait qu'elle s'écroule dans un grand fracas et dans une gerbe d'étincelles dues à une mauvaise connexion électrique qui projette maintenant des éclairs bleutés chargés de centaines de milliers de volts.";

  res.render('text', {
    message: string,
    image_url : 'images/black-b.jpg',
    link: link,
    text_buttom: text_buttom,
    statistique : {
      stats_habilite : req.query.habilite,
      stats_endurance :  req.query.endurance,
      stats_protection :  req.query.protection,
      stats_chance :  req.query.chance,
    },
    object : {
      // ?habilite=10&endurance=12&protection=8&chance=6
      weapon : {
        slot01_weapon : req.query.sl_w_01,
        slot02_weapon :  req.query.sl_w_02,
        slot03_weapon :  req.query.sl_w_03,
        slot04_weapon :  req.query.sl_w_04,
        slot05_weapon :  req.query.sl_w_05,
        slot06_weapon :  req.query.sl_w_06,
      },
      items : {
        slot01_item : req.query.sli01,
        slot02_item :  req.query.sli02,
        slot03_item :  req.query.sli03,
        slot04_item :  req.query.sli04,
        slot05_item :  req.query.sli05,
        slot06_item :  req.query.sli06,
      },
    },
  });

});


module.exports = router;
