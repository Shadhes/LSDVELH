var express = require('express');
var router = express.Router();
var confs = require('../configs')

/* GET home page. */
router.get('/', function(req, res, next) {
  link = 'banana';
  string = "L'appareil consiste en une boîte de forme carrée et de couleur noire, muni d'un gros bouton rouge en son centre. Les fils le relient à une source d'énergie. Vous pressez le bouton, mais il ne se passe rien. Comme l'appareil comporte de nombreux jacks vides, vous en concluez qu'il est incomplet. Vous le glissez néanmoins dans votre sac. Laissant là votre macabre découverte, allez-vous vous diriger vers l'écoutille de gauche, ou vers celle de droite ?";
  var stats = '?habilite=' + req.query.habilite + '&endurance=' + req.query.endurance + '&protection=' + req.query.protection + '&chance=' + req.query.chance + '';
  var items = '&sli01=' + req.query.sli01 + '&sli02=' + req.query.sli02 + '&sli03=' + req.query.sli03 + '&sli04=' + req.query.sli04 + '&sli05=' + req.query.sli05 + '&sli06=' + req.query.sli06 + '';

  res.render('standardChoice',{
    message: string,
    image_url : 'images/page141/page141_01.png',
    choice01 : {
      text : 'Ecoutille de droite',
      link : '/page077' + stats + items,
      image_url : false,
    },
    choice02 : {
      text : 'Ecoutille de gauche',
      link : '/page058' + stats + items,
      image_url : false,
    },
    choice03 : {
      text : '',
      link : '',
      image_url : false,
    },
    choice04 : {
      text : '',
      link : '',
      image_url : false,
    },
    choice05 : {
      text : '',
      link : '',
      image_url : false,
    },
    choice06 : {
      text : '',
      link : '',
      image_url : false,
    },
    link : link,
    statistique : {
      stats_habilite : req.query.habilite,
      stats_endurance :  req.query.endurance,
      stats_protection :  req.query.protection,
      stats_chance :  req.query.chance,
    },
    object : {
      // ?habilite=10&endurance=12&protection=8&chance=6
      weapon : {
        slot01_weapon : req.query.slw01,
        slot02_weapon :  req.query.slw02,
        slot03_weapon :  req.query.slw03,
        slot04_weapon :  req.query.slw04,
        slot05_weapon :  req.query.slw05,
        slot06_weapon :  req.query.slw06,
      },
      items : {
        slot01_item : req.query.sli01,
        slot02_item :  req.query.sli02,
        slot03_item :  req.query.sli03,
        slot04_item :  req.query.sli04,
        slot05_item :  req.query.sli05,
        slot06_item :  req.query.sli06,
      },
    },
  });

});

module.exports = router;
