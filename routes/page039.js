var express = require('express');
var router = express.Router();
var confs = require('../configs')

/* GET home page. */
router.get('/', function(req, res, next) {
  link = 'banana';
  string = "Vous vous approchez du tas de détritus, et vous constatez qu'il s'agit, en fait, des restes d'une créature extra-terrestre, bossue et de petite taille. Des haillons, qui ont dû être autrefois des vêtements, couvrent encore sa peau, et ses six membres osseux sont recroquevillés sous son corps. Une traînée de substance verdâtre indique qu'elle a certainement franchi l'écoutille de droite avant de venir mourir ici. Vous retournez le cadavre du pied, et vous remarquez que l'une des mains est crispée sur un appareil électronique. Un doigt semble encore en presser le bouton, et des fils métalliques en sortent pour disparaître dans un restant de manche de la créature. Allez-vous vous emparer de cet étrange appareil ou, redoutant un danger quelconque, préférez-vous vous en désintéresser ?";

  var stats = '?habilite=' + req.query.habilite + '&endurance=' + req.query.endurance + '&protection=' + req.query.protection + '&chance=' + req.query.chance + '';
  var items = '&sli01=' + req.query.sli01 + '&sli02=' + req.query.sli02 + '&sli03=' + req.query.sli03 + '&sli04=' + req.query.sli04 + '&sli05=' + req.query.sli05 + '&sli06=' + req.query.sli06 + '';
  var items_drop = '&sli01=boite_avec_jacks_vides&sli02=' + req.query.sli02 + '&sli03=' + req.query.sli03 + '&sli04=' + req.query.sli04 + '&sli05=' + req.query.sli05 + '&sli06=' + req.query.sli06 + '';

  res.render('standardChoice',{
    message: string,
    image_url : 'images/page039/page039_01.jpg',
    choice01 : {
      text : 'Continuer sans prendre la boite',
      link : '/page234' + stats + items,
      image_url : false,
    },
    choice02 : {
      text : 'S’emparer de la boite',
      link : '/page141' + stats + items_drop,
      image_url : false,
    },
    choice03 : {
      text : '',
      link : '',
      image_url : false,
    },
    choice04 : {
      text : '',
      link : '',
      image_url : false,
    },
    choice05 : {
      text : '',
      link : '',
      image_url : false,
    },
    choice06 : {
      text : '',
      link : '',
      image_url : false,
    },
    link : link,
    statistique : {
      stats_habilite : req.query.habilite,
      stats_endurance :  req.query.endurance,
      stats_protection :  req.query.protection,
      stats_chance :  req.query.chance,
    },
    object : {
      // ?habilite=10&endurance=12&protection=8&chance=6
      weapon : {
        slot01_weapon : req.query.sl_w_01,
        slot02_weapon :  req.query.sl_w_02,
        slot03_weapon :  req.query.sl_w_03,
        slot04_weapon :  req.query.sl_w_04,
        slot05_weapon :  req.query.sl_w_05,
        slot06_weapon :  req.query.sl_w_06,
      },
      items : {
        slot01_item : req.query.sli01,
        slot02_item :  req.query.sli02,
        slot03_item :  req.query.sli03,
        slot04_item :  req.query.sli04,
        slot05_item :  req.query.sli05,
        slot06_item :  req.query.sli06,
      },
    },
  });

});

module.exports = router;
