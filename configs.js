var statistiques = {
  stats_habilite :  10,
  stats_endurance :  12,
  stats_protection :  08,
  stats_chance :  04,
  object : {
        // ?habilite=10&endurance=12&protection=8&chance=6
        weapon : {
          slot01_weapon :  '',
          slot02_weapon :  '',
          slot03_weapon :  '',
          slot04_weapon :  '',
          slot05_weapon :  '',
          slot06_weapon :  '',
        },
        items : {
          slot01_item :  '',
          slot02_item :  '',
          slot03_item :  '',
          slot04_item :  '',
          slot05_item :  '',
          slot06_item :  '',
        },
      },
};
module.exports = statistiques;
